import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'Weather.dart';

class NetworkingPage extends StatefulWidget {
  const NetworkingPage({Key? key}) : super(key: key);

  @override
  State<NetworkingPage> createState() => _NetworkingPageState();
}

class _NetworkingPageState extends State<NetworkingPage> {

  String _location = "";
  String _dateTime = "";
  String _numberOfDay = "";

  Future<Map<String, dynamic>> fetchApi() async {
    final response = await http.get(Uri.parse('http://worldtimeapi.org/api/timezone/Asia/Jakarta'));
    Map<String, dynamic> extractedData = jsonDecode(response.body);
    return extractedData;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Networking and Integration'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text(
              'Learning about navigation, networking, and integration'
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: const Text('Go back!'),
            ),
            const SizedBox(
              height: 50,
            ),
            Text(
                'Location: $_location'
            ),
            Text(
                'Date & time: $_dateTime'
            ),
            Text(
                'Day of year: $_numberOfDay'
            ),
            ElevatedButton(
              onPressed: () async {
                var apiMap = await fetchApi();
                setState(() {
                  _location = apiMap['timezone'];
                  _dateTime = apiMap['datetime'];
                  _numberOfDay = apiMap['day_of_year'].toString();
                });
              },
              child: const Text('Get API'),
            )
          ],
        ),
      ),
    );
  }
}
