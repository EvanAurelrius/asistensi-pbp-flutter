import 'package:flutter/material.dart';

class CalculatorForm extends StatefulWidget {
  const CalculatorForm({Key? key}) : super(key: key);

  @override
  State<CalculatorForm> createState() => _CalculatorFormState();
}

class _CalculatorFormState extends State<CalculatorForm> {

  final _formKey = GlobalKey<FormState>();
  int _result = 0;
  final firstNumberCon = TextEditingController();
  final secondNumberCon = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          const Text(
            'Input your first number:'
          ),
          SizedBox(
            width: 300,
            child: TextFormField(
              keyboardType: TextInputType.number,
              validator: (value) {
                if (value==null || value.isEmpty) {
                  return "First number can't be empty!";
                }
              },
              controller: firstNumberCon,
              decoration: const InputDecoration(
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black, width: 1),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.blue, width: 1),
                ),
                errorBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.red, width: 1),
                ),
                focusedErrorBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.red, width: 1),
                ),
              ),
            ),
          ),
          const Text(
              'Input your second number:'
          ),
          SizedBox(
            width: 300,
            child: TextFormField(
              keyboardType: TextInputType.number,
              validator: (value) {
                if (value==null || value.isEmpty) {
                  return "Second number can't be empty!";
                }
              },
              controller: secondNumberCon,
              decoration: const InputDecoration(
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black, width: 1),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.blue, width: 1),
                ),
                errorBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.red, width: 1),
                ),
                focusedErrorBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.red, width: 1),
                ),
              ),
            ),
          ),
          const SizedBox(
            height: 50,
          ),
          Text(
              'Result: $_result'
          ),
          const SizedBox(
            height: 50,
          ),
          ElevatedButton(
            onPressed: () {
              if(_formKey.currentState!.validate()) {
                FocusScopeNode currentFocus = FocusScope.of(context);
                if (!currentFocus.hasPrimaryFocus) {
                  currentFocus.unfocus();
                }
                  setState(() {
                    _result = int.parse(firstNumberCon.text) + int.parse(secondNumberCon.text);
                  });
              }
            },
            child: const Text('Calculate!'),
          )
        ],
      ),
    );
  }
}
