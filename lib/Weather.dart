class Weather {
  late String location;
  late String datetime;
  late int dayOfYear;

  Weather(this.location, this.datetime, this.dayOfYear);

  factory Weather.fromJson(dynamic json) {
    return Weather(json['timezone'] as String, json['datetime'] as String, json['dayofyear'] as int);
  }

  @override
  String toString() {
    return '{ ${this.location}, ${this.datetime}, ${this.dayOfYear.toString()}';
  }
}